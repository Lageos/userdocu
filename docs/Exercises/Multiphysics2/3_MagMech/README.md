# HW3: Magentic Mechanic


### Description
Consider an axi-symmetric model of a coil and a plate as depicted in the figure.
The inner and outer radii of the coil, $r_1$ = 0.0025 m and $r_2$ = 0.0075 m.
The length of the coil, $l_1$ = 0.008 m.
The plate thickness and air gap is $l_2$ = 5 $\cdot 10^{-4}$ m respectively $l_3$ = 7.5 $\cdot 10^{-4}$ m.
The point $C$ is located in the center of the coil, the point P in the middle of the plate at $z$ = 0.005 m.
The plate is either made from iron or aluminum, with electric conductivity and linear magnetic permeability given in the `mat.xml`.
The coil has 40 turns and is fed by a controlled current of $i$ = 10 kA. 

![sketch](sketch.png){: style="width:200px"}

---------------------------

### Tasks
1. Create a regular hexaedral mesh using Trelis. **(1 Points)**
2. Create input-xml files for the required CFS++ simulations. **(1 Points)**
3. Document your results by answering the questions below. **(13 Points)**

### Hints
- Use the provided templates [`coil_transient.xml`](coil_transient.xml), [`mat.xml`](mat.xml) and [`HW5_template.ipynb`](HW5_template.ipynb).
- Adapt the Trelis input [`geometry.jou`](geometry.jou).
- Use the static simulation to check the prescribed current and boundary conditions. 
- Use a time resolution of 20 steps per period.
- Pole force can be neglected.

### Submission
Submit all your input files (`*.jou`, `*.xml`) as well as a concise PDF report of your results. Name the archive according to the format `HWn_eNNNNNNN_Lastname.zip`.

---------------------------

### Mesh Size
Estimate the skin depth for both materials. What element size do you suggest? Motivate your suggestion! **(3 Points)**

### Static Analysis
Compute the static solution of the coupled magnetic-mechanical problem.

- Plot the vector field of the magnetic flux density. **(2 Points)**
- Compare the cases for iron and aluminum plate. **(2 Points)**
- What is the difference? **(3 Points)**
- Are there displacements? Why / Why not? **(3 Points)**

### Transient Analysis 
Set up a transient analysis with a sinusoidal coil current with a frequency of 100 Hz and solve the coupled magnetic-mechanical problem. Compute the solution for 4 periods of the coil current.

- Plot the time signal of the z-displacements of point $P$ for both plate-materials in one figure
- Plot the displacement of the plate (paraview warp by vector) for both plate-materials.
- Describe the time signals: Are there transient effects? What is the difference between the amplitudes with aluminum and iron plate and why is there a difference?

---------------------------

You can download the templates linked on this page individually by _right click --> save target as_.
Alternativey, head over to the [git repository](https://gitlab.com/openCFS/userdocu/-/tree/master/docs/Exercises/Multiphysics2) and use the _download button_ on the top right: 
Here is a direct link to the [__zip archive__](https://gitlab.com/openCFS/userdocu/-/archive/master/HW3.zip?path=docs/Exercises/Multiphysics2/3_MagMech).