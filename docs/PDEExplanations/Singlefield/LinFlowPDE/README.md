# LinFlow PDE

###Governing equations
Sound propagation in small dimensions, where the viscous boundary layer is comparable to the feature dimensions, is affected by the viscous losses. LinFlow PDE is proposed to model these viscous losses. 
The governing equation for LinFlow PDE contains a linearized balance of mass and momentum for a viscous compressible or incompressible fluid.
To use the compressible formulations in openCFS, set the ``` formulation ``` to ``` compressible``` in xml-simulation file.
Otherwise, by default, the formulation will be solved for incompressible fluids $(\nabla \cdot \mathbf v = 0 )$. LinFlow PDE is defined in xml-simulation file as ``` fluidMechLin ```

``` 
<fluidMechLin  formulation = "compressible">
```
The linearized balance of mass and momentum are

\begin{eqnarray}
\frac{1}{c^2} \frac{\partial p}{\partial t}
+\mathbf \nabla \cdot (\rho_0 \mathbf v)  = 0 \,,\\
\rho_0 \frac{\partial \mathbf v}{\partial t} - \mathbf \nabla \cdot \mathbf \sigma = \mathbf  0\,. \label{eq:momentumAcou}
\end{eqnarray}

Where $p$, $\rho$, and $\mathbf v$ are the thermodynamic pressure, density, and velocity, respectively.
$c$ is the wave speed, $\rho$ is the fluid density, and $\mathbf\sigma$ is the fluid stress tensor.
In the above formulations, we assumed that there was no background velocity.
We assumed the pressure and the density variations are linearly related ($p = \rho c^2$), and no external forces exist in momentum conservation.
The $\mathbf\sigma$ for isotropic Newtonian fluid with no locally preferred direction is written as 

\begin{equation}
\mathbf\sigma
=  -p \mathbf I 
+\mu \big (\mathbf \nabla \mathbf v+(\mathbf \nabla \mathbf v)^T \big)
+(\lambda-\frac{2}{3} \mu) (\mathbf \nabla \cdot \mathbf v)\mathbf I,
\end{equation}

Where $\mu$ is dynamic (shear) viscosity and $\lambda$ is the bulk viscosity. 
To drive the weak forms, the strong forms of the balance of mass \eqref{eq:massAcou} and momentum \eqref{eq:momentumAcou} are multiplied by appropriate test functions and integrated over the whole computational domain $\Omega$. 
Then, by applying Green's theorem, the weak forms derived as

\begin{eqnarray}
\int\limits_{\Omega} p' \mathbf \nabla \cdot \mathbf v {\mathrm {d}} \Omega  +
\int\limits_{\Omega} p' \frac{1}{c^2} \frac{\partial p}{\partial t} {\mathrm {d}} \Omega = 0,
\end{eqnarray}

\begin{equation}
\int\limits_{\Omega} \rho \mathbf v' \cdot \frac{\partial \mathbf v}{\partial t}\, {\mathrm {d}} \Omega +
\int\limits_{\Omega} \mathbf \nabla \mathbf v' : \mathbf \sigma {\mathrm {d}} \Omega - 
\int\limits_{\Gamma} (\mathbf v' \cdot \mathbf \sigma) \cdot \mathbf n {\mathrm {d}} \Gamma = \mathbf 0.
\label{eq:thermoviscous-momentum_weak}
\end{equation}

In these equations, $\mathbf v'$ and $ p’$ are fluid velocity and pressure test functions, respectively.
The $\mathbf n$ is the surface normal vector, and $\Gamma$ is the boundary surfaces.
To meet the inf-sup condition, it is recommended to use a one-order higher polynomial shape function for velocity than that for the pressure. 

To dedicate these orders in the simulation-xml file, tags ``` presPolyId ``` for pressure and ``` velPolyId ``` for velocity polynomial orders are defined.

```
<fluidMechLin  formulation = "" presPolyId = "" velPolyId = "">
```
### Boundary conditions
Defining a wall as a boundary in LinFlow PDE is the most common boundary condition. This homogeneous Dirichlet boundary condition is defined by setting the velocity to zero. 

\begin{equation}
\mathbf v = 0 \, .
\end{equation}

The natural (homogeneous Neumann) boundary condition in LinFlow PDE means the normal stress (traction) is zero. 

\begin{equation}
\mathbf \sigma \cdot \mathbf n  = \mathbf 0 \, .
\end{equation}

openCFS provides the following boundary conditions 

```
	<bcsAndLoads>
	   <velocity name="">
	        <comp dof="" value=""/>
	    </velocity>
	   <noSlip name="">
	        <comp dof=""/>
	   </noSlip>
	   <normalSurfaceMass name="" value="" volumeRegion=""/>
	   <pressure name="" value=""/>
	   <noPressure name=""/>
	   <traction name="">
	        <comp dof="" value=""/>
	   </traction>
	   <normalTraction name="" value="" volumeRegion=""/>
	   <absorbingBCs name="" volumeRegion=""/>
	   <normalImpedance name="" value="" volumeRegion=""/>
	</bcsAndLoads>
```
* **velocity**: Inhomogeneous Dirichlet boundary condition by assigning value $v_D$.

* **noslip**: Homogeneous Dirichlet boundary condition by assigning zero value for velocity at certain ``` <comp dof=""/>```.

* **normalSurfaceMass**: Adds an inertia term for movement in the normal direction of boundary surfaces.
It will "constrain" normal motion to zero on the boundary if the value is "high enough," i.e., it becomes a penalty parameter. This boundary condition is useful if the boundary is not aligned to coordinate axes (where Dirichlet conditions can be directly applied) or when the boundaries are carves. <!--This is accomplished by a bi-linear form $\int_\Gamma m \mathbf v' \cdot \mathbf n  \frac{\partial \mathbf v}{\partial t}\ \cdot \mathbf n {\mathrm {d}}  \Gamma$ on the surface, where the parameter $m$ represents a mass per area --> 
* **pressure**: Assigns pressure value at the boundary as an excitation.
* **noPressure**: Assigns zero pressure value (soundsoft BC).

* **traction**: Inhomogeneous Neumann boundary condition by assigning non-zero value.

* **normalTraction**: Normal inhomogeneous Neumann boundary condition by assigning non-zero value. This boundary condition is useful when the boundary is not aligned to coordinate axes or when the boundaries are carves. 

* **normalImpedance**: Specifies an impedance boundary condition by 
\begin{equation}
[-p \mathbf I 
+\mu(\nabla \mathbf v+(\nabla \mathbf v)^T)
+(\lambda-\frac{2}{3} \mu) (\nabla \cdot \mathbf v) \mathbf I] \cdot \mathbf n = 
z_0 (\mathbf v \cdot \mathbf n) \cdot \mathbf n \,.
\label{eq:impedance}
\end{equation}
This boundary condition can also create wall and no-pressure boundary conditions depending on the specified impedance value (high and low, respectively).

* **absorbingBCs**: Applies complex impedance of fluid instead of specified impedance ($z_0$) at the boundary.

### Material

A material-xml file for the LinFlow PDE is described below. Same as acoustic field ([AcousticPDE](../AcousticPDE)), density $\rho$, compression modulus $K$ are defiend and openCFS uses these material properties for computing the speed of sound
\begin{equation}
c = \sqrt{\frac{K}{\rho}} \, .
\end{equation}
In addition to the acoustic materials, for an inviscid fluid, the shear $\mu$ and bulk $\lambda$ viscosities should be defined for computing the viscous stress.

```
  <material name="Air"> 
    <flow>
      <density>
        <linear>
          <real> 1.225 </real>
        </linear>
      </density>
      <compressionModulus>
        <linear>
          <real> 1.4271e5 </real>
        </linear>
      </compressionModulus>
      <dynamicViscosity>
        <linear>
          <real> 1.829e-5 </real>
        </linear>
      </dynamicViscosity>
      <bulkViscosity>
        <linear>
          <real> 1.22e-5 </real>
        </linear>
      </bulkViscosity>
      <adiabaticExponent>
        <linear>
          <real> 1.406 </real>
        </linear>
      </adiabaticExponent>
    </flow>
  </material>
```
The adiabatic exponet enters the formulation in heat coupling ([LinFlowHeatPDE](../../Coupledfield/LinFlowHeatPDE)).

## Analysis Types
Since, in general, we are dealing with a time-dependent PDE, we can distinguish three analysis types:

* **Harmonic** 

* **Eigenfrequency**
```
        <analysis> 
	    <eigenValue>
        	<valuesAround>
        		<shiftPoint>
	        		<Real></Real>
	        		<Imag></Imag>
        		</shiftPoint>
        		<number></number>
        	</valuesAround>
        	<eigenVectors normalization="none" side="right"/>
        	<problemType>
	        	<Generalized>
		        	<aMatrix>stiffness</aMatrix>
		        	<bMatrix>damping</bMatrix>
	        	</Generalized>
        	</problemType>
            </eigenValue>
        </analysis>
```
*not validated yet:*
 **Transient**


## Postprocessing results

The following results can be computed using openCFS and the LinFlow PDE.

**Defined on nodes**

* **Fluid mechanics pressure**:
```
<nodeResult type="fluidMechPressure">
```

* **Fluid mechanics velocity**:
```
<nodeResult type="fluidMechVelocity">
```

# References
\bibliography

